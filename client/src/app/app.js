var bookmarksApp = angular.module('bookmarksApp', ['ngRoute']);

bookmarksApp.config(['$routeProvider', function($routeProvider) {
    $routeProvider
        .when('/', {
            template: ' ',
            controller: 'BookmarksController'
        })
        .when('/filter/:filterTag', {
            template: ' ',
            controller: 'BookmarksController'
        })
        .otherwise({
            redirectTo: '/'
        });
}]);

bookmarksApp.controller('BookmarksController', ['$scope', '$routeParams', function ($scope, $routeParams) {

    $scope.bookmarks = [
        {url: 'http://jquery.com', title: "jQuery1", tags: ["JavaScript","jQuery","Library"]},
        {url: 'http://underscorejs.org', title: "UnderscoreJS", tags: ["JavaScript","Library","Underscore"]},
        {url: 'http://backbonejs.org', title: "Backbone", tags: ["Backbone","JavaScript"]}];

    $scope.routeParams = $routeParams;

    $scope.addBookmark = function () {
        var tags = $scope.tags.split(",");
        tags = tags.map(function(e){return e.trim();}) // remove whitespaces around tags
        tags = tags.filter(function(n){ return n != "" }); // remove empty tags
        tags = tags.filter(function(item, i, ar){ return ar.indexOf(item) === i; }); // remove duplicates
        var bookmark = {url: $scope.url, title: $scope.title, tags: tags};
        var isEdit = $scope.index >= 0;
        if (isEdit) {
            $scope.bookmarks[$scope.index] = bookmark;
        } else { // new bookmark
            $scope.bookmarks.push(bookmark);
        }
        $scope.formClear();
    };

    $scope.delete = function (bookmark) {
        var index = $scope.bookmarks.indexOf(bookmark);
        $scope.bookmarks.splice(index, 1);
    }

    $scope.edit = function (bookmark) {
        var index = $scope.bookmarks.indexOf(bookmark);
        $scope.index = index;
        $scope.url = $scope.bookmarks[index].url;
        $scope.title = $scope.bookmarks[index].title;
        $scope.tags = $scope.bookmarks[index].tags.toString();
    }

    $scope.formClear = function() {
        $scope.index = -1;
        $scope.url='';
        $scope.title='';
        $scope.tags=''
    }

    $scope.getAllTags = function() {
        tags = []
        $scope.bookmarks.forEach(function(bookmark) {
            bookmark.tags.forEach(function(tag) {
                if (tags.indexOf(tag) == -1) {
                    tags.push(tag);
                }
            });
        });
        return tags;
    }

}]);
